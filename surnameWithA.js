function surnameWithA(data) {
  try {
    const surnamesOfPeopleWithA = data.reduce((accumulator, house) => {
      let initialValue = house.people[0];

      house.people.reduce((accumulator1, person) => {
        let namesSplitArray = person.name.split(" ");
        let surname = namesSplitArray[namesSplitArray.length - 1];

        if (surname.toLowerCase()[0] === "a") {
          accumulator.push(person.name);
        }
      }, initialValue);

      return accumulator;
    }, []);
  } catch (error) {
    console.log(error);
  }

  return surnamesOfPeopleWithA;
}

module.exports = surnameWithA;
