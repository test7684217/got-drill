function surnameWithS(data) {
  try {
    const surnamesOfPeopleWithS = data.reduce((accumulator, house) => {
      let initialValue = house.people[0];

      house.people.reduce((accumulator1, person) => {
        let namesSplitArray = person.name.split(" ");
        let surname = namesSplitArray[namesSplitArray.length - 1];

        if (surname.toLowerCase()[0] === "s") {
          accumulator.push(person.name);
        }
      }, initialValue);

      return accumulator;
    }, []);

    return surnamesOfPeopleWithS;
  } catch (error) {
    console.log(error);
  }
}

module.exports = surnameWithS;
