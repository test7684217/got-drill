function everyone(data) {
  try {
    let ans = data.reduce((accumulator, house) => {
      let initialValue = house.people[0];

      house.people.reduce((accumulator1, person) => {
        accumulator.push(person.name);
      }, initialValue);

      return accumulator;
    }, []);

    return ans;
  } catch (error) {
    console.log(error);
  }
}

module.exports = everyone;
