function nameWithA(data) {
  try {
    const namesOfPeopleWithA = data.reduce((accumulator, house) => {
      house.people.reduce((accumulator1, people) => {
        let name = people.name;

        if (name.toLowerCase().includes("a")) {
          accumulator.push(people.name);
        }
      });

      return accumulator;
    }, []);

    return namesOfPeopleWithA;
  } catch (error) {
    console.log(error);
  }
}

module.exports = nameWithA;
