function countAllPeople(data) {
  try {
    let countOfAllPeople = data.reduce((accumulator, currentValue) => {
      accumulator += currentValue.people.length;
      return accumulator;
    }, 0);

    return countOfAllPeople;
  } catch (error) {
    console.log(error);
  }
}

module.exports = countAllPeople;
