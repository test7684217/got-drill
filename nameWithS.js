function nameWithS(data) {
  try {
    const namesOfPeopleWithS = data.reduce((accumulator, house) => {
      let initialValue = house.people[0];

      house.people.reduce((accumulator1, people) => {
        let name = people.name;

        if (name.toLowerCase().includes("s")) {
          accumulator.push(people.name);
        }
      }, initialValue);

      return accumulator;
    }, []);
  } catch (error) {
    console.log(error);
  }

  return namesOfPeopleWithS;
}

module.exports = nameWithS;
