function peopleByHouses(data) {
  try {
    let peopleByHousesObject = data.reduce((accumulator, currentValue) => {
      accumulator[currentValue.name] = currentValue.people.length;
      return accumulator;
    }, {});

    return peopleByHousesObject;
  } catch (error) {
    console.log(error);
  }
}

module.exports = peopleByHouses;
