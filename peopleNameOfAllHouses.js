function peopleNameOfAllHouses(data) {
  try {
    let peopleNameOfAllHouses = data.reduce((accumulator, house) => {
      
      accumulator[house.name] = house.people.reduce((accumulator1, person) => {
        accumulator1.push(person.name);
        return accumulator1;
      }, []);

      return accumulator;

    }, {});

    return peopleNameOfAllHouses;
  } catch (err) {
    console.log(err);
  }
}

module.exports = peopleNameOfAllHouses;
